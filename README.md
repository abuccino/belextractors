# BELextractors

SpikeInterface extractors for BEL chips. For more information about SpikeInterface,
refere to the [documentation](https://spikeinterface.readthedocs.io/en/latest/).

## Installation

Clone the repository and install as follows:

```bash
git clone https://git.bsse.ethz.ch/abuccino/belextractors.git
cd belextractors 
pip install .
```

## Usage

From Python, you can instantiate recording extractors for the Mea1k and 
DualMode chips:

```python
import belextractors as be

recording_mea1k = be.Mea1kRecordingExtractor('path-to-mea1k-h5')
recording_dm = be.DualModeRecordingExtractor('path-to-dualmode-h5')
```

For the Mea1k file, you can also load a sorting extractor with spikes detected by
channel:

```python
sorting_mea1k = be.Mea1kSortingExtractor('path-to-mea1k-h5')
```


