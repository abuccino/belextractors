from spikeinterface.core import BaseRecording, BaseRecordingSegment
import probeinterface as pi
from pathlib import Path
import numpy as np

try:
    import h5py
    from scipy.io import loadmat
    HAVE_H5PY = True
except ImportError:
    HAVE_H5PY = False


class NeuroCMOSRecordingExtractor(BaseRecording):
    """
    SpikeInterface recording extractor for NeuroCMOS chip.

    Parameters
    ----------
    file_path: str or Path
        Path to the neurocmos h5 file
    stream_id: str
        If multi stream, the stream to load ("ephys0", "ephys1", etc.)

    Returns
    -------
    recording: NeuroCMOSRecordingExtractor
        The spikeinterface recording

    Examples
    --------

    >>> import belextractors as be
    >>>
    >>> recording = be.NeuroCMOSRecording('path-to-neurocmos.h5')
    >>>
    >>> # get channel ids
    >>> channel_ids = recording.get_channel_ids()
    """
    extractor_name = 'NeuroCMOSRecording'
    has_default_locations = True
    has_unscaled = True
    installed = HAVE_H5PY  # check at class level if installed or not
    is_writable = True
    mode = 'file'
    installation_mesg = "To use the NeuroCMOSRecordingExtractor install h5py and scipy: " \
                        "\n\n pip install h5py scipy\n\n"  # error message when not installed

    def __init__(self, file_path, stream_id=None):
        assert HAVE_H5PY, self.installation_mesg
        self._file_path = file_path

        h5 = h5py.File(self._file_path, mode='r')
        self.h5_file = h5

        # ephys
        ephys = self.h5_file["ephys"]
        if any("ephys" in ephys_key for ephys_key in ephys.keys()):
            assert stream_id is not None, f"Select one stream id between: {list(ephys.keys())}"
            assert stream_id in list(ephys.keys()), f"Select one stream id between: {list(ephys.keys())}"
            ephys_group = ephys[stream_id]
        else:
            ephys_group = ephys

        fs = ephys_group["frame_rate"][()]

        maps = list(ephys_group["configs"].keys())
        if len(maps) > 1:
            print(f"Found more than one mapping. Using {maps[0]}")
        map_name = maps[0]
        self.mapping = ephys_group["configs"][map_name][:]
        sig = ephys_group["signal"]

        channel_ids = self.mapping["channel_number"]
        electrodes = self.mapping["electrode"]
        x = self.mapping["x_um"]
        y = self.mapping["y_um"]

        # remove unused channels
        routed_idxs = np.where(electrodes > -1)[0]
        channel_ids = channel_ids[routed_idxs]
        electrodes = electrodes[routed_idxs]
        x = x[routed_idxs]
        y = y[routed_idxs]

        if sig.shape[0] > len(channel_ids):
            channel_slice = channel_ids
        else:
            channel_slice = None

        BaseRecording.__init__(self, fs, channel_ids, dtype="uint16")

        # gain is fixed to 2000 (for now)
        gain = 2000
        gain_uV = 2 / (1024 * gain) * 1e6

        # set properties
        locations = np.array([x, y]).T
        # make probeinterface probe
        shapes = "rect"
        shape_params = {'width': 5, 'height': 9}

        probe = pi.Probe(ndim=2, si_units='um')
        probe.set_contacts(positions=locations,
                           shapes=shapes, shape_params=shape_params)
        probe.create_auto_shape(probe_type="rect")
        probe.set_device_channel_indices(np.arange(self.get_num_channels()))

        self.set_property('electrode', electrodes)
        self.set_channel_gains(gain_uV)
        self.set_channel_offsets(0)

        rec_segment = NeuroCMOSRecordingSegment(file_path, channel_slice, stream_id)
        self.add_recording_segment(rec_segment)
        self.set_probe(probe, in_place=True)

        self._kwargs = {'file_path': str(Path(file_path).absolute())}


class NeuroCMOSRecordingSegment(BaseRecordingSegment):
    def __init__(self, file_path, channel_slice, stream):
        BaseRecordingSegment.__init__(self)

        self.h5_file = h5py.File(file_path, mode='r')
        if stream is not None:
            self._signals = self.h5_file["ephys"][stream]["signal"]
        else:
            self._signals = self.h5_file["ephys"]["signal"]
        self._num_frames = self._signals.shape[1]
        self._channel_slice = channel_slice

    def get_num_samples(self):
        """Returns the number of samples in this signal block

        Returns:
            SampleIndex: Number of samples in the signal block
        """
        return self._num_frames

    def get_traces(self,
                   start_frame=None,
                   end_frame=None,
                   channel_indices=None,
                   ) -> np.ndarray:
        sigs = self._signals

        if start_frame is None:
            start_frame = 0
        if end_frame is None:
            end_frame = self._num_frames

        if self._channel_slice is not None:
            sigs = sigs[self._channel_slice, start_frame:end_frame]
            sigs = sigs[channel_indices]
        else:
            sigs = sigs[channel_indices, start_frame:end_frame]
        return sigs.T


def read_neurocmos(*args, **kwargs):
    return NeuroCMOSRecordingExtractor(*args, **kwargs)


read_neurocmos.__doc__ = NeuroCMOSRecordingExtractor.__doc__