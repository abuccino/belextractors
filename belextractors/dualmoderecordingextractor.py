from spikeinterface.core import BaseRecording, BaseRecordingSegment
import probeinterface as pi
from pathlib import Path
import numpy as np

try:
    import h5py
    from scipy.io import loadmat
    HAVE_DUAL = True
except ImportError:
    HAVE_DUAL = False

this_file_path = Path(__file__)
this_parent_path = this_file_path.parent

matlab_params_file = this_parent_path / 'aps_array.mat'


class DualModeRecordingExtractor(BaseRecording):
    """
    SpikeInterface recording extractor for DualMode chip.

    Parameters
    ----------
    file_path: str or Path
        Path to the dualmode file converted to h5

    Returns
    -------
    recording: DualModeRecordingExtractor
        The spikeinterface recording

    Examples
    --------

    >>> import belextractors as be
    >>>
    >>> recording = be.DualModeRecordingExtractor('path-to-dualmode.h5')
    >>>
    >>> # get electrode ids
    >>> electrode_ids = recording.get_electrode_ids()
    """
    extractor_name = 'DualModeRecording'
    has_default_locations = True
    has_unscaled = True
    installed = HAVE_DUAL  # check at class level if installed or not
    is_writable = True
    mode = 'file'
    installation_mesg = "To use the DualModeRecordingExtractor install h5py and scipy: " \
                        "\n\n pip install h5py scipy\n\n"  # error message when not installed

    def __init__(self, file_path):
        assert HAVE_DUAL, self.installation_mesg
        self._file_path = file_path

        fs, lsb, locations, channels, electrodes = parse_matlab_params(matlab_params_file)

        # remove unused channels
        routed_idxs = np.where(electrodes > -1)[0]
        self._all_channels = list(channels)
        self._num_all_channels = len(self._all_channels)
        self._electrode_ids = list(electrodes[routed_idxs])
        channel_ids = list(channels[routed_idxs])

        BaseRecording.__init__(self, fs, channel_ids, dtype="float32")

        # set properties
        locations = locations[routed_idxs]
        # make probeinterface probe
        shapes = "square"
        shape_params = {'width': 8, 'height': 12}

        probe = pi.Probe(ndim=2, si_units='um')
        probe.set_contacts(positions=locations,
                           shapes=shapes, shape_params=shape_params)
        probe.create_auto_shape(probe_type="rect")
        probe.set_device_channel_indices(np.arange(self.get_num_channels()))

        # self.set_channel_locations(locations[routed_idxs])
        self.set_property('electrode', self._electrode_ids, ids=channel_ids)
        self.set_channel_gains(lsb * 1e6)
        rec_segment = DualModeRecordingSegment(file_path, electrodes, channels)

        self.add_recording_segment(rec_segment)
        self.set_probe(probe, in_place=True)

        self._kwargs = {'file_path': str(Path(file_path).absolute())}

    def get_electrode_ids(self):
        return list(self._electrode_ids)

    @staticmethod
    def get_all_locations():
        matlab_params_file = this_parent_path / 'aps_array.mat'
        fs, lsb, locations, channels, electrodes = parse_matlab_params(matlab_params_file)
        return locations

    @staticmethod
    def get_all_electrode_ids():
        matlab_params_file = this_parent_path / 'aps_array.mat'
        fs, lsb, locations, channels, electrodes = parse_matlab_params(matlab_params_file)
        return list(electrodes)

    @staticmethod
    def get_all_channel_ids():
        matlab_params_file = this_parent_path / 'aps_array.mat'
        fs, lsb, locations, channels, electrodes = parse_matlab_params(matlab_params_file)
        return list(channels)


class DualModeRecordingSegment(BaseRecordingSegment):
    def __init__(self, file_path, electrodes, channels):
        BaseRecordingSegment.__init__(self)

        self._filehandle = h5py.File(file_path, mode='r')

        # remove unused channels
        self._all_channels = list(channels)
        routed_idxs = np.where(electrodes > -1)[0]
        self._channels = list(channels[routed_idxs])
        self._num_channels = len(channels[routed_idxs])
        self._num_all_channels = len(channels)

        self._n_aps = len(self._filehandle.keys())
        self._n_dout = len(self._filehandle['APS0'].keys())
        self._num_frames, self._num_chans_per_dout = self._filehandle['APS0']['dout0'].shape

    def get_num_samples(self):
        """Returns the number of samples in this signal block

        Returns:
            SampleIndex: Number of samples in the signal block
        """
        return self._num_frames

    def get_traces(self,
                   start_frame=None,
                   end_frame=None,
                   channel_indices=None,
                   ) -> np.ndarray:
        traces = np.zeros((end_frame - start_frame, self._num_all_channels))

        # Find idxs of all channels corresponding to channel_indices
        channel_idxs = np.array([self._all_channels.index(ch) for ch in self._channels[channel_indices]])
        # Retrieve all channels
        n_chan = 0
        for aps in range(self._n_aps):
            for dout in range(self._n_dout):
                traces[:, n_chan:n_chan + self._num_chans_per_dout] = \
                    - self._filehandle[f'APS{aps}'][f'dout{dout}'][start_frame:end_frame]
                n_chan += self._num_chans_per_dout
        # slice
        return traces[:, channel_idxs]


def read_dualmode(*args, **kwargs):
    return DualModeRecordingExtractor(*args, **kwargs)


read_dualmode.__doc__ = DualModeRecordingExtractor.__doc__


def parse_matlab_params(mat_params_file):
    info = loadmat(mat_params_file)
    ntk = info['ntk']
    fs = _squeeze_ds(ntk['sr'])
    lsb = _squeeze_ds(ntk['lsb'])
    x = ntk['x2'][0][0][0]
    y = ntk['y2'][0][0][0]
    channel_ids = ntk['channel_nr'][0][0][0].astype('int')
    electrode_ids = ntk['el_idx2'][0][0][0].astype('int')
    locations = np.array([x, y]).T

    return fs, lsb, locations, channel_ids, electrode_ids


def _squeeze_ds(ds):
    while not isinstance(ds, (int, float, np.integer, np.float)):
        ds = ds[0]
    return ds
