# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

d = {}
exec(open("belextractors/version.py").read(), None, d)
version = d['version']
long_description = open("README.md").read()

entry_points = None

install_requires = []

setup(
    name="belextractors",
    version=version,
    author="Alessio Buccino",
    author_email="alessiop.buccino@gmail.com",
    description="SpikeInterface extractors for BEL at ETH",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://git.bsse.ethz.ch/abuccino/belextractors",
    install_requires=[
        'numpy',
        'h5py',
        'scipy',
        'spikeextractors>=0.9.4'
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
    ],
    packages=find_packages(),
    entry_points=entry_points,
    include_package_data=True,
)
